FROM ubuntu:jammy
# General-purpose packages used by common setups
RUN apt-get update && DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get install -y \
	git \
	gnupg \
	openssh-client \
	python3-boto \
	python3-boto3 \
	python3-botocore \
	python3-cryptography \
	python3-docker \
	python3-netaddr \
	python3-pip \
	python3-tz \
	python-is-python3 \
	wget
# Zerotier support
RUN wget -qO - https://raw.githubusercontent.com/zerotier/ZeroTierOne/master/doc/contact%40zerotier.com.gpg | apt-key add - && \
	echo "deb http://download.zerotier.com/debian/jammy jammy main" >> /etc/apt/sources.list && \
	apt-get update && \
	apt-get install -y zerotier-one
# PIP packages including Ansible and some common dependencies
RUN pip install \
	ansible \
	ansible-lint \
	ara \
	dnspython \
	pynetbox
