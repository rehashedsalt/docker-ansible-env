# docker-ansible-env

A Docker container to run my Ansible playbook shenanigans

## Volatility Warning

This repository makes no effort to control the version of Ansible installed. When a new version of Ansible releases and breaks my stuff, I want it to be *loud*. I want to fix my stuff.

I may add tags in the future based on the installed Ansible version, or at least based on build time.

## Usage

```bash
docker run -it rehashedsalt/ansible-env:bleeding /bin/bash
```

Mount the Ansible configuration into the container and fire it. Additional configuration may be required based on the repository.

## Contents

* Ansible
* Ansible-lint
* OpenSSH and related components, such as gnupg
* Python libraries for Docker and Netbox
* PIP
* wget
* Zerotier

## Zerotier Configuration

Configuring and starting Zerotier during runtime is fairly straightforward, albeit you have to introduce some delays to allow the application to start up properly. Here's some template code:

```bash
service zerotier-one start
sleep 5
zerotier-cli join $MY_NETWORK_ID
sleep 5
zerotier-cli info
zerotier-cli listnetworks
```

Be sure and leave afterward:

```bash
zerotier-cli leave $MY_NETWORK_ID
```
